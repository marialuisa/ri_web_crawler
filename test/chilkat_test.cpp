#include "chilkat_client.hpp"
#include "test.hpp"
#include "link.hpp"
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <chrono>
#include <iostream>
#include <future>
#include <thread>
#include <cassert>

#include <mutex>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

#define TIMER 25000

int to_download(char url[MAX_URL_SIZE], int timer_url){
	ChilkatClient client;
	std::this_thread::sleep_for(std::chrono::milliseconds(timer_url));
	std::string html = client.Download(url);
	return 0;
}

int can_download(string path_server_urls_downladed, string url){

	char line_downloaded[MAX_URL_SIZE];

	/* primeiro temos q criar o arquivo de paginas baixadas se ele ainda nao existe */
	ifstream obj2(path_server_urls_downladed.c_str());
	if(!obj2){ std::ofstream{ path_server_urls_downladed.c_str() }; }else{
		/* Abre o arquivo das urls q ja foram lidas */
		FILE *file_downloaded;
		file_downloaded = fopen(path_server_urls_downladed.c_str(), "r");

		/* se o arquivo nao eh nulo percorre as urls ja baixadas */
		if(file_downloaded == NULL){ fclose(file_downloaded); return 1; } else {
			while((fgets(line_downloaded, sizeof(line_downloaded), file_downloaded)) != NULL){
				/* confere se a url a ser baixada esta no arquivo das urls ja baixadas */
				if(strcmp(url.c_str(), line_downloaded) == 0){
					// // printf("ja baixou %s\n", url.c_str());
					return 0;
				}
			}
		}
		/* Fecha o arquivo das urls q ja foram lidas */
		fclose(file_downloaded);
	}
	return 1;
}

int main() {

	/*
	 *
	 *	criar as subpastas necessarias para o projeto rodar
	 * 	Estrutura de dados hash implementada em arquivos
	 *
	 *
	 */
	struct stat st;
	string folder = "files";
	string folder_to_download = "files/to_download";
	string folder_downloaded = "files/downloaded";
	string folder_output = "output";
	if (stat(folder.c_str(), &st) != 0)	mkdir(folder.c_str(), 0700);
	if (stat(folder_to_download.c_str(), &st) != 0)	mkdir(folder_to_download.c_str(), 0700);
	if (stat(folder_downloaded.c_str(), &st) != 0)	mkdir(folder_downloaded.c_str(), 0700);
	if (stat(folder_output.c_str(), &st) != 0)	mkdir(folder_output.c_str(), 0700);
	
	/* cria o arquivo de saida onde as paginas serao salvas */
	string out_path = (folder_output + std::string("/pages_download"));
	ifstream obj(out_path.c_str());
	if(!obj) std::ofstream{ out_path.c_str() };

	
	const char * test_urls[] = { 
		"www.saraiva.com.br", "www.sbt.com.br", "www.vagalume.com.br",
		"https://www.sitegratisgratis.com.br/", "https://www.kadaza.com.br", "https://www.olhardigital.com.br/", "https://www.abril.com.br/", "https://www.fatosdesconhecidos.com.br", "https://www.otvfoco.com.br/", "http://www.saopaulo.sp.gov.br/", "http://www.caixa.gov.br",
		"https://www.reclameaqui.com.br/", "https://www.bol.uol.com.br/", "http://www.olx.com.br/", "https://www.submarino.com.br/", "https://www.curapelanatureza.com.br/", "https://www.santander.com.br/", "https://www.canaltech.com.br/", "https://eleicoes.uol.com.br/", "https://www.kabum.com.br/", "http://mg.gov.br/",
		"http://www.infomoney.com.br/", "http://www.bb.com.br/", "http://www.gazetadopovo.com.br/", "https://www.designerd.com.br/", "http://designculture.com.br/", "http://designconceitual.com.br/", "http://www.cafecomgalo.com.br/", "https://livros.gospelmais.com.br/", "www.carlosromero.com.br/", "https://www.codigofonte.com.br/", "https://www.hastedesign.com.br/", "https://bjr.sbpjor.org.br/", "http://www.marketingdigitalconsultor.com.br/", "https://jornalggn.com.br", "https://www.cartacapital.com.br/", "http://doctela.com.br/", "http://www.scielo.br/", "http://www.rankingdesites.com.br/", "http://bibliotecadigital.fgv.br", "https://www.voxus.com.br/", "https://imasters.com.br/", "www.convergenciadigital.com.br/", "http://revistaseletronicas.pucrs.br/",
		"http://www.minhaserie.com.br/", "http://apaixonadosporseries.com.br", "https://www.uai.com.br", "http://portaldopurus.com.br", "http://www.nutrinew.com.br", "http://portalradar.com.br", "https://oportaln10.com.br", "https://terradomandu.com.br", "https://macmagazine.com.br", "http://www.jcuberaba.com.br", "http://www.portaldoholanda.com.br", "http://www.bandab.com.br", "http://diarioarapiraca.com.br", "http://www.uxdesignstudio.com.br", "https://portalcorreio.com.br", "https://www.portalego.com.br", "https://www.portaldofranchising.com.br", "http://www.pontojovemnet.com.br/", "http://www.portalmarcossantos.com.br", "http://www.danilofiuza.com.br/"
	};

	ChilkatClient client;
	CkSpider spider;
	// for (auto& url : test_urls) {
	// 	const char *n_url = spider.canonicalizeUrl(url);
	// 	// // printf("%s\n", n_url);
	// 	std::string html = client.Download(n_url); 
	// }
	
	// /* variareis */
	int value_sz_url = 3, j = 0;
	char name_server[MAX_URL_SIZE], pth_list_servers[100] = "servers_list";
	FILE *file_list_servers;
	
	string urls_t_download[2000000];
	// /* quantidade de threads */
	int q_thread = 10000000;
	std::thread *m_threads = (std::thread*) malloc(sizeof(std::thread) * q_thread);
	int v = 0, continuue = 1;
	int timer_url = TIMER;
	char new_url[10000][MAX_URL_SIZE];
	
	/* pegar lista de servidores */

		system("cd files/to_download/ && ls | sed 's/.br/.br/g' | sort | uniq > ../../servers_list");
		file_list_servers = fopen(pth_list_servers, "r");
		if(file_list_servers != NULL){

		while(continuue == 1){		

			j = 0;

			/* Le cada servidor */
			system("cd files/to_download/ && ls | sed 's/.br/.br/g' | sort | uniq > ../../servers_list");
			int have_urls;
			file_list_servers = fopen(pth_list_servers, "r");
			while((fgets(name_server, sizeof(name_server), file_list_servers)) != NULL){
				have_urls = 0;
				// // printf("%s", name_server);
				name_server[strlen(name_server)-1] = '\0';

				value_sz_url = 3;

				while(value_sz_url < 8 && have_urls == 0){

					/* leitura do arquivo das urls daquela tamanho na pasta do servidor	*/
					std::string path_server_urls_to_download = "files/to_download/" + std::string(name_server)+ std::string("/") + std::to_string(value_sz_url);
					std::string path_server_urls_downladed = "files/downloaded/" + std::string(name_server)+ std::string("/") + std::to_string(value_sz_url);

					/* primeiro temos q criar o arquivo de paginas a baixar se ele ainda nao existe */
					ifstream obj(path_server_urls_to_download.c_str());
					if(obj){				
						FILE *file_f;
						file_f = fopen(path_server_urls_to_download.c_str(), "r");

						/* se o arquivo nao eh nulo percorre as urls a serem baixadas */
						if(file_f == NULL){ fclose(file_f); } else {
							char line_to_download[MAX_URL_SIZE];
							std::string vector_url[2000000];
							
							int row = 0;
							if(v == 0) timer_url = 0;

							while(((fgets(line_to_download, sizeof(line_to_download), file_f)) != NULL)){
								line_to_download[strlen(line_to_download)-1] = '\0';
								if(row < 2000000)
									vector_url[row] = std::string(line_to_download);
								row++;
							}

							if(row > 0){
								have_urls = 1;
								char * tst;
								// strcpy(tst, vector_url[0].c_str());
								if(can_download(path_server_urls_downladed, vector_url[0]) == 1){
									// m_threads[j] = std::thread(to_download, tst, timer_url);
									urls_t_download[j] = vector_url[0];

									strcpy(new_url[j], urls_t_download[j].c_str());
									m_threads[j] = std::thread(to_download, new_url[j], timer_url);
									printf("%s\n", new_url[j]);
									j++;


								}
							}

							int f = 0;
							remove(path_server_urls_to_download.c_str());
							ifstream obj2(path_server_urls_to_download.c_str());
							if(!obj2) std::ofstream{ path_server_urls_to_download.c_str() };
							for(f = 1; f < row; f++){
								/* coloca a url no arquivo do servidor */
								std::ofstream file;
								file.open(path_server_urls_to_download.c_str(), ios::out | ios::app);
								file << spider.canonicalizeUrl(vector_url[f].c_str()) << std::endl;
								file.close(); 
							}
						}
						fclose(file_f);
					}
					if(have_urls == 0){
						value_sz_url++;
					}
				}
			} // fecha a leitura de cada servidor 
			fclose(file_list_servers);

			// if(j >= 1000){
				int x = 0;
				for (x = 0; x < j; x++)
				{
					m_threads[x].join();
				}
				// j = 0;
			// }
			v++;
		}


		}
	
	return 0;
}
