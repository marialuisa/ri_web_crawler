#include "chilkat_client.hpp"
#include "test.hpp"
#include "link.hpp"
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <chrono>
#include <iostream>
#include <future>
#include <thread>
#include <cassert>

#include <mutex>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

#define TIMER 30000

/*
 * 
 * 	Funcao auxiliar que pega um dominio e um tamanho e baixa todas as urls daquele dominio
 * 
 * 	@param server  		nome do servidor
 * 	@param size_url		tamanho da url
 * 
 * 
 */
int schedule(const std::string& server, int size_url) {

	printf("%s %d\n", server.c_str(), size_url);

	ChilkatClient client;
	string path_server_urls = "files/to_download/" + std::string(server)+ std::string("/") + std::to_string(size_url);
	string path_server_urls_downladed = "files/downloaded/" + std::string(server)+ std::string("/") + std::to_string(size_url);


	/* leitura do arquivo das urls daquela tamanho na pasta do servidor	*/

	/* primeiro temos q criar o arquivo de paginas baixadas se ele ainda nao existe */
	ifstream obj(path_server_urls.c_str());
	if(!obj){}
	else{

	
		FILE *file_f;
		file_f = fopen(path_server_urls.c_str(), "r");
		char line_to_download[MAX_URL_SIZE], line_downloaded[MAX_URL_SIZE];
		

		/* se o arquivo nao eh nulo percorre as urls a serem baixadas */
		if(file_f == NULL){ fclose(file_f); return 0; } else {
			while((fgets(line_to_download, sizeof(line_to_download), file_f)) != NULL){
				int can_down = 1;
				
				printf("%s %d\n", server.c_str(), size_url);

				/* primeiro temos q criar o arquivo de paginas baixadas se ele ainda nao existe */
				ifstream obj(path_server_urls_downladed.c_str());
				if(!obj) std::ofstream{ path_server_urls_downladed.c_str() };
				else{
					/* Abre o arquivo das urls q ja foram lidas */
					FILE *file_downloaded;
					file_downloaded = fopen(path_server_urls_downladed.c_str(), "r");

					/* se o arquivo nao eh nulo percorre as urls ja baixadas */
					if(file_downloaded == NULL){ fclose(file_downloaded); fclose(file_f); return 0; } else {
						while((fgets(line_downloaded, sizeof(line_downloaded), file_downloaded)) != NULL){
							/* confere se a url a ser baixada esta no arquivo das urls ja baixadas */
							if(strcmp(line_to_download, line_downloaded) == 0){
								can_down = 0;
							}
						}
					}

					/* Fecha o arquivo das urls q ja foram lidas */
					fclose(file_downloaded);
				}

				/* confere se a url ainda nao foi baixada, caso possa baixar, o download eh feito */
				if(can_down == 1){
					printf("vai baixar: %s\n", line_to_download);
					std::string html = client.Download(line_to_download);
					printf("terminou a funcao: %s\n", line_to_download);
					std::this_thread::sleep_for(std::chrono::milliseconds(TIMER));
				}else{
					printf("%s Url ja foi baixada\n", line_to_download);
				}
			}
		}
		fclose(file_f);
		/* exclui o arquivo das paginas a serem baixadas, pois o download ja foi feito */
		remove(path_server_urls.c_str());
	
	}
	return 0;
}


int to_download(char url[MAX_URL_SIZE], int timer_url){
	ChilkatClient client;
	std::this_thread::sleep_for(std::chrono::milliseconds(timer_url));
	std::string html = client.Download(url);
	return 0;
}

int can_download(string path_server_urls_downladed, string url){

	char line_downloaded[MAX_URL_SIZE];

	/* primeiro temos q criar o arquivo de paginas baixadas se ele ainda nao existe */
	ifstream obj2(path_server_urls_downladed.c_str());
	if(!obj2){ std::ofstream{ path_server_urls_downladed.c_str() }; }else{
		/* Abre o arquivo das urls q ja foram lidas */
		FILE *file_downloaded;
		file_downloaded = fopen(path_server_urls_downladed.c_str(), "r");

		/* se o arquivo nao eh nulo percorre as urls ja baixadas */
		if(file_downloaded == NULL){ fclose(file_downloaded); return 1; } else {
			while((fgets(line_downloaded, sizeof(line_downloaded), file_downloaded)) != NULL){
				/* confere se a url a ser baixada esta no arquivo das urls ja baixadas */
				if(strcmp(url.c_str(), line_downloaded) == 0){
					printf("ja baixou %s\n", url.c_str());
					return 0;
				}
			}
		}
		/* Fecha o arquivo das urls q ja foram lidas */
		fclose(file_downloaded);
	}
	return 1;
}

int main() {


	/*
	 *
	 *	criar as subpastas necessarias para o projeto rodar
	 * 	Estrutura de dados hash implementada em arquivos
	 *
	 *
	 */
	struct stat st;
	string folder = "files";
	string folder_to_download = "files/to_download";
	string folder_downloaded = "files/downloaded";
	string folder_output = "output";
	if (stat(folder.c_str(), &st) != 0)	mkdir(folder.c_str(), 0700);
	if (stat(folder_to_download.c_str(), &st) != 0)	mkdir(folder_to_download.c_str(), 0700);
	if (stat(folder_downloaded.c_str(), &st) != 0)	mkdir(folder_downloaded.c_str(), 0700);
	if (stat(folder_output.c_str(), &st) != 0)	mkdir(folder_output.c_str(), 0700);
	
	/* cria o arquivo de saida onde as paginas serao salvas */
	string out_path = (folder_output + std::string("/pages_download"));
	ifstream obj(out_path.c_str());
	if(!obj) std::ofstream{ out_path.c_str() };


	/* pega os resultados das primeiras urls */
	const char * test_urls[] = { 
		"www.campograndenews.com.br", "www.sbt.com.br", "www.vagalume.com.br"
	};

	ChilkatClient client;
	CkSpider spider;
	// for (auto& url : test_urls) {
	// 	const char *n_url = spider.canonicalizeUrl(url);
	// 	printf("%s\n", n_url);
	// 	std::string html = client.Download(n_url); 
	// }
	
	// /* variareis */
	int value_sz_url = 3, j = 0;
	char name_server[MAX_URL_SIZE], pth_list_servers[100] = "servers_list";
	FILE *file_list_servers;
	

	// /* quantidade de threads */
	int q_thread = 10000000;
	std::thread *m_threads = (std::thread*) malloc(sizeof(std::thread) * q_thread);
	int v = 0;
	
	/* pegar lista de servidores */

		system("cd files/to_download/ && ls | sed 's/.br/.br/g' | sort | uniq > ../../servers_list");
		file_list_servers = fopen(pth_list_servers, "r");
		if(file_list_servers != NULL){

		while(value_sz_url < 8){
			int have_urls = 0;
			j = 0;
			printf("\n---------------------------- tamanho %d ----------------------------\n\n", value_sz_url);
			
			/* Le cada servidor */
			system("cd files/to_download/ && ls | sed 's/.br/.br/g' | sort | uniq > ../../servers_list");
			file_list_servers = fopen(pth_list_servers, "r");
			while((fgets(name_server, sizeof(name_server), file_list_servers)) != NULL){
				printf("%s", name_server);
				name_server[strlen(name_server)-1] = '\0';

				/* leitura do arquivo das urls daquela tamanho na pasta do servidor	*/
				string path_server_urls_to_download = "files/to_download/" + std::string(name_server)+ std::string("/") + std::to_string(value_sz_url);
				string path_server_urls_downladed = "files/downloaded/" + std::string(name_server)+ std::string("/") + std::to_string(value_sz_url);


				/* primeiro temos q criar o arquivo de paginas baixadas se ele ainda nao existe */
				ifstream obj(path_server_urls_to_download.c_str());
				if(obj){				
					FILE *file_f;
					file_f = fopen(path_server_urls_to_download.c_str(), "r");
					char line_to_download[MAX_URL_SIZE];
					string vector_url[2000000];

					/* se o arquivo nao eh nulo percorre as urls a serem baixadas */
					if(file_f == NULL){ fclose(file_f); } else {
						int row = 0;
						int timer_url = TIMER;
						if(v == 0) timer_url = 0;

						while(((fgets(line_to_download, sizeof(line_to_download), file_f)) != NULL)){
							line_to_download[strlen(line_to_download)-1] = '\0';
							printf("%s\n", line_to_download);
							vector_url[row] = std::string(line_to_download);
							row++;
						}


						if(row > 0){
							have_urls = 1;
							char * tst;
							strcpy(tst, vector_url[0].c_str());

							if(can_download(path_server_urls_downladed, vector_url[0]) == 1){
								m_threads[j] = std::thread(to_download, tst, timer_url);
								j++;
							}
						}
						int f = 0;

						// string cmd_clear = "cat clear_file > " + std::string(path_server_urls_to_download);
						// system(cmd_clear.c_str());
						remove(path_server_urls_to_download.c_str());
						ifstream obj2(path_server_urls_to_download.c_str());
						if(!obj2) std::ofstream{ path_server_urls_to_download.c_str() };

						for(f = 1; f < row; f++){
							/* coloca a url no arquivo do servidor */
							std::ofstream file;
							file.open(path_server_urls_to_download.c_str(), ios::out | ios::app);
							file << spider.canonicalizeUrl(vector_url[f].c_str()) << std::endl;
							file.close(); 
						}
					}
					fclose(file_f);
				}
			} // fecha a leitura de cada servidor 
			fclose(file_list_servers);
			
			if(have_urls == 0){
				value_sz_url++;
				printf("nao teve urls\n");
			}else{
				printf("teve urls\n");
			}

			int x =0;
			for(x = 0; x < j; x++){
				m_threads[x].join();
			};

			printf("%d\n", value_sz_url);
			v++;
		}


		}
	
	return 0;
}
