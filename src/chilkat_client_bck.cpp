#include "chilkat_client.hpp"
#include "link.hpp"

#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

std::string ChilkatClient::Download(const std::string& url) {

	using std::cout;
	using std::endl;
 	
 	// printf("\n%s\n", url.c_str());
	Link n_link(strdup(url.c_str()));
 
	CkSpider spider;
	spider.AddUnspidered(url.c_str());
	if (!spider.CrawlNext()) {
		CkString error;
		spider.LastErrorText(error);
		// throw std::runtime_error(error.getString()); 
	}

	std::string html = spider.lastHtml();
	std::vector<std::string> queue; 

	printf("to_download: %d\n\n", spider.get_NumUnspidered()); 
	
	// Inbound links.
	for (int i = 0; i < spider.get_NumUnspidered(); i++) {

			std::string link = spider.getUnspideredUrl(0);
	 
			printf("\nurl:    %s\n", link.c_str());
			Link n_link(strdup(link.c_str()));
			printf("size:   %d\n", n_link.szLink);
			printf("domain: %s\n", n_link.domain);

			spider.SkipUnspidered(0);
 
			// so pega as urls validas  
			if(n_link.valid == 1){

				printf("url OK\n");

				// cria a pasta do dominio se nao existe ainda
				system("mkdir -p files/to_download");
				char comand[150] = "cd files/to_download && touch ";
				char *cmd_pth; 	 
				cmd_pth = strcat(comand, n_link.domain);		
				system(cmd_pth);

				char comand_p1[MAX_COMMAND_LINE_SIZE] = "echo ";
				char comand_p2[MAX_COMMAND_LINE_SIZE] = ">> files/to_download/";
				char comand_p3[MAX_COMMAND_LINE_SIZE] = " && sort files/to_download/";
				char comand_p4[MAX_COMMAND_LINE_SIZE] = " | uniq > aux && cat aux > files/to_download/";
				cmd_pth = strcat(comand_p1, n_link.url);
				cmd_pth = strcat(cmd_pth, comand_p2);
				cmd_pth = strcat(cmd_pth, n_link.domain);
				cmd_pth = strcat(cmd_pth, comand_p3); 
				cmd_pth = strcat(cmd_pth, n_link.domain);
				cmd_pth = strcat(cmd_pth, comand_p4);
				cmd_pth = strcat(cmd_pth, n_link.domain);
				// printf("%s\n", cmd_pth);
				system(cmd_pth);
		 
				queue.push_back(link);
		
			}else{
				printf("url invalida!\n");
			}
	}

	// Outbound links.
	for (int i = 0; i < spider.get_NumOutboundLinks(); i++) {
		std::string link = spider.getOutboundLink(i);
		queue.push_back(link);
	}

	// printf("%d\n", sizeof(char));
	// char *cmd_pth;
	// char comand_p1[MAX_COMMAND_LINE_SIZE] = "echo \\|\\|\\|";
	// cmd_pth = strcat(comand_p1, n_link.url);
	// char comand_p5[MAX_COMMAND_LINE_SIZE] = "\\|";
	// cmd_pth = strcat(comand_p1, comand_p5);
	// cmd_pth = strcat(cmd_pth, html.c_str());
	// // char comand_p6[MAX_COMMAND_LINE_SIZE] = "";
	// // cmd_pth = strcat(cmd_pth, comand_p6);
	// printf("%s\n", cmd_pth);
	// printf("%s\n", strcat("echo", html.c_str()));
	// system(cmd_pth);

	char cmd_p1[MAX_COMMAND_LINE_SIZE] = ">> ";
	char cmd_p2[MAX_COMMAND_LINE_SIZE] = "files/proccess/";
	// char cmd_p3[MAX_COMMAND_LINE_SIZE] = "rm ";

	char *cmd_pth; 
	cmd_pth = strcat(cmd_p2, n_link.url);
	
	std::ofstream out;
	// out.open("file/process");
	out.open(cmd_pth);
	
	cmd_pth = strcat(cmd_p1, cmd_p2);

	system(cmd_pth);

	// char pipes[5] = "|";
	// char *str_fnl;

	// str_fnl = strcat(pipes, pipes);
	// str_fnl = strcat(str_fnl, pipes);
	// str_fnl = strcat(str_fnl, pipes);

	// out << str_fnl << endl;
	out << html << endl;

	// char cmd_p4[MAX_COMMAND_LINE_SIZE] = "cat files/proccess/";
	// char cmd_p5[MAX_COMMAND_LINE_SIZE] = " | sed 's/|//g' >>";
	// char cmd_p6[MAX_COMMAND_LINE_SIZE] = "_iln";

	// cmd_pth = strcat(cmd_p4, n_link.url);
	// cmd_pth = strcat(cmd_pth, cmd_p5);
	// cmd_pth = strcat(cmd_pth, n_link.url);
	// cmd_pth = strcat(cmd_pth, cmd_p6);

	// apenas para colocar o codigo em uma linha so
	char cmd_p4[MAX_COMMAND_LINE_SIZE] = "./files/proccess/_script.sh ";
	cmd_pth = strcat(cmd_p4, n_link.url);
	system(cmd_pth);
	// printf("%s\n", cmd_pth);

	// cmd_pth = strcat(cmd_pth, "| sed ':a;N;s/\n//g;ta >> ");
	// cmd_pth = strcat(cmd_pth, n_link.url);

	// system(cmd_pth);
	// system("cat output/pages | sed ':a;N;s/\n//g;ta >> ");

	return html;
}

	// char m_url[MAX_URL_SIZE] = "http://dcc.ufmg.br/dcc/";
	// char **urls = (char**) malloc(7 * sizeof(char**));
	// int i = 0, sz = 0;
	// int *bf = (int*) malloc(sizeof(int) * 2);
	// bf = &sz;

	// for(i = 0; i < 7; i++) {
	//     urls[i] = (char*) malloc(14 * sizeof(char));
	// }
	// // *urls[0] = ( char * ) malloc ( sizeof ( char ) * ( 14 ) );

	// sz = separateUrl(m_url, bf, urls);

	// // printf("%d\n", *bf);

	// // for(i = 0; i < 7; i++) {
	// //     printf("s: %s\n", urls[i]);
	// // }