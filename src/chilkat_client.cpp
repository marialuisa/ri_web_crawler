#include "chilkat_client.hpp"
#include "link.hpp"

#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include <mutex>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

int _pages = 0;
std::mutex mtx;

/*
 * 
 * 	Funcao auxiliar que salva a url nas pastas
 * 
 * 	@param n_link 		url a ser salva
 * 	@param type_link 	tipo de url, 1: para baixar, 2: ja baixada
 * 
 * 
 */
void saveUrl(Link n_link, int type_link){

	CkSpider spider;
	/* cria a pasta do dominio se nao existe ainda */
	struct stat st;

	mtx.lock();
		std::string folder;
		if(type_link == 2)
			folder = "files/downloaded/" + std::string(n_link.domain);
		else
			folder = "files/to_download/" + std::string(n_link.domain);

		if (stat(folder.c_str(), &st) != 0) mkdir(folder.c_str(), 0700);
				
	 	/* cria o arquivo de cada tamanho de url no servidor */ 
		string file_name = std::to_string(n_link.szLink);
		string path = (folder + std::string("/") + std::string(file_name));
		ifstream obj(path.c_str());
		if(!obj) std::ofstream{ path.c_str() };
		
	 	/* coloca a url no arquivo do servidor */
		std::ofstream file;
		file.open(path.c_str(), ios::out | ios::app);
		file << n_link.url << std::endl;
		file.close(); 
		
		string name_file_ax = "output/ax"+ std::string(n_link.domain) + std::to_string(n_link.szLink) + std::string(n_link.parts[0]) + std::string(n_link.parts[1]) + std::string(n_link.parts[2]);
		string sort_cmd = "sort " + path + " | uniq > "+ name_file_ax + " && cat "+ name_file_ax +" > " + path;
		system(sort_cmd.c_str());
		remove(name_file_ax.c_str());
	mtx.unlock();
	
}

/*
 * 
 * 	Funcao que faz a validacao do html antes de baixa-lo
 * 
 * 	@param url  		url a ser baixada
 * 
 * 
 */
bool ValidateHtml(const std::string& html) {
	unsigned int max_size = 2097152;
	return ((html.find("<html"  ) != std::string::npos) || html.find("<body"  ) != std::string::npos || html.find("<p"  ) != std::string::npos) || sizeof(html) <= max_size;
}


/*
 * 
 * 	Funcao que faz o download da pagina 
 * 
 * 	@param url  		url a ser baixada
 * 
 * 
 */
std::string ChilkatClient::Download(const std::string& url) {

	std::string r = "success";

	// printf("baixando: %s\n", url.c_str());

	std::mutex mtx;
	CkSpider spider;
	using std::cout;
	using std::endl;
	
	// /* Cria a estrutura da url */
	Link n_link_parent(strdup(url.c_str())); 
 
	spider.AddUnspidered(url.c_str());
	if (!spider.CrawlNext()) {
		CkString error;
		spider.LastErrorText(error);
	}

	std::string html = spider.lastHtml();


	// if(n_link_parent.valid == 1){
			
		// Inbound links.
		for (int i = 0; i < spider.get_NumUnspidered(); i++) {
			std::string link = spider.getUnspideredUrl(0);
			Link n_link(strdup(link.c_str()));
			spider.SkipUnspidered(0);
 
			// so pega as urls validas  
			if(n_link.valid == 1){
				// saveUrl(n_link, 1);	 
			}
		}
		// Outbound links.
		for (int i = 0; i < spider.get_NumOutboundLinks(); i++) {	
				std::string link2 = spider.getOutboundLink(i);				
				Link n_out_link(strdup(link2.c_str()));
				if(n_out_link.valid){
					saveUrl(n_out_link, 3);
				}			
			
		}

	//  	/* validar html */
	// 	// if(ValidateHtml(html) == true){

			/*
			 *	Salva no arquivo de saida
			 *
			 *
			 */

			mtx.lock();
			// abre o arquivo de saida
			std::string out_path = "output/pages_download";
			std::ofstream out;
			out.open(out_path, ios::out | ios::app);
			

			// coloca o header da pagina
			std::string head_page =  std::string("||| ") + spider.canonicalizeUrl(n_link_parent.url) +  std::string(" | ");
			out << head_page << endl; 


			// retira os pipes da variavel url e a coloca em uma linha 		
			std::replace(html.begin(), html.end(), '|', ' ');
			// std::replace(html.begin(), html.end(), '\n', ' ');
			// std::replace(html.begin(), html.end(), '\r', ' ');
			// std::replace(html.begin(), html.end(), '\t', ' ');
			// string::size_type pos = 0; 
			// while ((pos = html.find("\r\n\t",pos)) != string::npos) html.erase(pos, 2);

			out << html << endl; 

			out.close(); 


			// salva na pasta dos ja baixados 
			// saveUrl(n_link_parent, 2);
			_pages++;

			mtx.unlock();
	// 	// }
		
	// }else{
	// 	// // printf("url invalida\n");
	// }
	return r;
}


