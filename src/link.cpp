#include "link.hpp"

#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include <iostream> 
 
/* 
 * 
 *  function Link    construtor da classe 
 * 
 * 
 */
Link::Link(char linkString[MAX_URL_SIZE]){
	strcpy(url, linkString);

	parts = (char**) malloc(500 * sizeof(char**));
	valid = 0;

	int i = 0, sz = 0, p_br = 1, j = 0, linkValid = 0, sizeUrl = strlen(linkString);
	int *bf = (int*) malloc(sizeof(int) * 200);
	char *bf_str = (char*) malloc(sizeof(char) * MAX_URL_SIZE * 200 +  sizeof("/"));

	bf = &sz; 
	szLink = 0;
	for(i = 0; i < 7; i++){
		parts[i] = (char*) malloc(MAX_PART_URL_SIZE * 500 * sizeof(char));
		for(j = 0; j < MAX_PART_URL_SIZE; j++){
			parts[i][j] = '\0';
		}
	}

	strcpy(bf_str, linkString);
	if(linkString[strlen(linkString)-1] != '/' && linkString[strlen(linkString)-1] != '.'){
		bf_str = strcat(bf_str, "/");
	}

	int b = 0;
	for(i=2; i < (sizeUrl-1); i++){
		if(bf_str[i] == '/'){ b++; }
		if(((bf_str[i+1] == '/' || bf_str[i+1] == '.') && bf_str[i] == 'r' && bf_str[i-1] == 'b' && bf_str[i-2] == '.') && b < 3){
			linkValid = 1; valid = 1;
		}
	}

	if(linkValid > 0)
		sz = separateUrl(bf_str, bf, parts);

	for(i = 0; i < 7; i++) {
		if (strcmp(parts[i], "") != 0 && strcmp(parts[i], " ") != 0 && strlen(parts[i]) > 0){
			if(parts[i][0] == 'b' && parts[i][1] == 'r' && parts[i][2] == '\0' ){
				p_br = i;
			}
			szLink++;
		}
	}

	if(*bf > 9 || *bf == -1)
		valid = 0;

	// define os dominios
	if(szLink > 2){
		strcpy(parts[p_br-2], strcat(parts[p_br-2], "."));
		strcpy(parts[p_br-1], strcat(parts[p_br-1], "."));
		strcpy(domain, strcat(parts[p_br-2], parts[p_br-1]));
		strcpy(domain, strcat(domain, parts[p_br]));
	}else{
		strcpy(domain, strcat(parts[p_br-1], parts[p_br]));
	}
}

/* 
 * 
 *  function separateUrl    funcao que pega o dominio pela string
 * 
 *  @param url          string da url 
 * 
 * 
 */
int Link::separateUrl(char url[MAX_URL_SIZE], int *p, char *urls[]){
	char * pch;
	char partUrl[MAX_URL_SIZE];
	int position = 0, i = 0, count = *p, sizeUrl, m = 0;



	for(i=0; i<MAX_URL_SIZE; i++) partUrl[i] = '\0';

	if(url[strlen(url)-1] != '/' && url[strlen(url)-1] != '.')
		url = strcat(url, "/");
	
	sizeUrl = strlen(url);

	// retira os pontos q nao fazem parte do dominio 
	if(*p < 1)
		for(i = 0; i < sizeUrl; i++){

			if(i > 1 && url[i] == 'r' && url[i-1] == 'b' && url[i-2] == '.' && (url[i+1] == '/' || url[i+1] == '.')){
				m++;
			}
			if(url[i] == '.' && m > 0)
				url[i] = '/';
			else if(url[i] == '/')
				url[i] = '.';
		}

	if(m > 0 || *p >= 1){

		// identifica se existe outra url passada como parametro como redirecionamento e trata o caso
		for(i = 0; i < sizeUrl-1; i++){
			if((*p > 3 && i > 1 && url[i] == 'w' && url[i-1] == 'w' && url[i-2] == 'w') || 
				(*p > 3 && i > 3 && url[i] == 'p' && url[i-1] == 't' && url[i-2] == 't' && url[i-3] == 'h')){
				url[i] = '.';
				url[i+1] = '\0';
				sizeUrl = i+1;
			}
		}
		// confere se o primeiro caractere eh um ponto
		if(url[0] == '.' && url[1] == '.'){
			for(i = 0; i < sizeUrl; i++)
				url[i] = url[i+2];
			url[sizeUrl-1] = '\0';
		}else if(url[0] == '.'){
			for(i = 0; i < sizeUrl; i++)
				url[i] = url[i+1];
			url[sizeUrl] = '\0';
		}

		// se existe um ponto
		pch = (char*) memchr (url, '.', sizeUrl);

		if(pch != NULL){
			position = pch - url + 1;

			// confere se nao eh a primeira parte da url
			if(*p > 0 && *p < 8){
				if (strcmp(url, "") != 0 && strcmp(url, " ") != 0 && strlen(url) > 0){
					for(i = 0; i < position; i++){
						if(url[i] != '.'){
							partUrl[i] = url[i];
							urls[count-1][i] = url[i];
						}
					}
					partUrl[position] = '\0';
					// printf("part: %s\n", partUrl);  
				}
			}
			strcpy(partUrl, "");

			count++;
			*p = count;
			
			if(strlen(pch) > 1)
				separateUrl(pch, p, urls);
			return (count);
		}
		else{
			return count;
		}
	}else{
		*p = -1;
	}
	return 0;

}


