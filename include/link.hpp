#include <string>
#include <iostream>

#define MAX_URL_SIZE 10000
#define MAX_PART_URL_SIZE 1500
#define MAX_COMMAND_LINE_SIZE 10000

class Link {
	public:
		int separateUrl(char url[MAX_URL_SIZE], int *p, char *urls[]);
	public:
		Link(char linkString[MAX_URL_SIZE]);
	public:
		int szLink;
	public:
		char **parts;
	public: 
		char domain[MAX_URL_SIZE];
	public:
		char url[MAX_URL_SIZE];
	public:
		int valid;
};
